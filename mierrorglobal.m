function [error] = mierrorglobal(met,f,f_exact,intv,y0,N)

% cálculo y con método
[T,Y] = met(f,intv,y0,N);

y_exact = f_exact(T);

error = max(max(abs(y_exact - Y)));

end
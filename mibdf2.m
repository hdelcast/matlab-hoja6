function [T,Y]=mibdf(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

t = a;
y = y0;

T = t;
Y = y;

% init Newton
f1 = f(t,y);
t = t + h;

F0 = @(z) z - (y + (h/2) * (f1 + f(t,z)));
Jf0 = @(z) eye(length(z)) - h * jfunc(z);

[y1,ev0,loop]=minewtonsystem(F0,Jf0,y,TOL,nmax);

Y = [ Y, y1(:,1) ];
T = [ T, t];

% bdf
for i=2:N

    % t
    t = t + h;
    
    % función auxiliar
    F = @(z) z - (4/3)*Y(:,i) + (1/3)*Y(:,i-1) - (2/3)*h*f(t,z);

    % jacobiano
    Jf = @(z) (2/3)*h*eye(length(z)) - (4/3)*h*jfunc(z);

    % iteración newton
    [y,ev0,loop]=minewtonsystem(F,Jf,y,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

end
end
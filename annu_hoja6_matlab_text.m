%% Prácticas de Matlab
%% Métodos multipaso
%% Hoja 6
% *Nombre: Hugo*
% 
% *Apellido: Del Castillo Mola*
% 
% *DNI:*
%% 
% %% Práctica 1 Ecuaciones en diferencias 
% Tomando como datos $x_{0}=1, \quad x_1=1.01,\quad N=100$, calculen los términos 
% de la sucesión
% 
% $$    \begin{cases}      x_{n+2} & = {7 \over 3} x_{n+1} - {2 \over 3} x_{n} 
% \\      & x_{0}, \ x_{1} \quad \mbox{dados}    \end{cases}$$
% 
% para $n=0, \ldots, N$. Los resultados han de almacenarse en la tabla $x$. 
% Además haz una gráfica de $x_{n}$ contra $n$.
% 
% *Solución:*

% Datos
x0=1;
x1=1.01;
N=100;
x=[x0;x1];

% Tabla x
for i=1:N-1
    x = [x; 7/3*x(end)-2/3*x(end-1)];
end

% Gráfica x_{n}
scatter(0:N,x,'filled')
s=sprintf("x0=%g x1=%g con N=%g",x0,x1,N);
title(s);


%% Práctica 2 Leap frog (Ecuación escalar)
% Consideramos el método de Leap-Frog (punto medio).
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerad la EDO
% 
% $$\begin{cases}y^{\prime}&=\lambda y\\y(0)&=1\\\end{cases}$$
% 
% $\lambda = -20$, resolved dicha EDO con el metodo de Leap-frog (usando el 
% método de Euler modificado para inicializarlo), con $N=100$, $N=1000$, y $N=10000$. 
% Pintad la solucion $y$ frente a $t$.
% 
% *Solución:*
%%

% Datos 
f = @(t,y) -20 * y;
y0=1;
N1=100;
N2=1000;
N3=10000;
intv=[0,1];

%% N=100
[T,Y] = mileapfrog(f,intv,y0,N1);
plot(T,Y);

%% N=1000
[T,Y] = mileapfrog(f,intv,y0,N2);
plot(T,Y);

%% N=10000
[T,Y] = mileapfrog(f,intv,y0,N3);
plot(T,Y);

%% Práctica 3 Leap frog (Sistemas de ecuaciones) 
% Consideramos el método de Leap-frog.
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
%% 
% * Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en 
% la hoja anterior 
%% 
% *Solución:*

% Datos
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
N0 = 100;

% vector N, h
nVec = [];
hVec = [];
for j=1:8
    nVec(j)= N0*2^(j);
    hVec(j)=10/nVec(j);
end 

% vector error
eVec=[];
for k=nVec
    eVec = [eVec, mierrorglobal(@mileapfrog,f,f_exact,intv,y0,k)];
end

% comprobación error
p2 = sprintf('%g ', eVec);
fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', 'mileapfrog', p2)

%plot
figure1 = figure;
lines = ["*-", "*-", ">-", "^-"];

figure(figure1);
grid on;
loglog(nVec,eVec,lines{1});
hold on;

%% 
% %% 
% * además  $N=1000$ dibuja el error  (es decir $\log\left(\|y(t_n)-y_n\|_{\infty}\right)$ 
% pero no $\log\left(\max(\max(|(y(t_n-y_n)|)))\right)$) frente la variable $t$.
%% 
% *Solución:*

N0 = 2000;

[T,Y] = mileapfrog(f,intv,y0,N0);

y_exact = f_exact(T);

error = log(max(abs(y_exact - Y)));

plot(T,error);

%% Práctica 5 BDF 
% Implementa el método *BDF*
% 
% $$  y_{n+2}-\frac{4}{3} y_{n+1}+\frac{1}{3} y_n =\frac{2}{3} h f_{n+2}\,.$$
% 
% *Observación:*
%% 
% * Inicializa el método con un método implícito del mismo orden.
% * En cada paso tienes que resolver una ecuación implícita  $z=g(h,x,z)$. Usa 
% la idea de iteración tipo Newton.
%% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en la 
% practica anterior
% 
% *Solución:*
%% Datos iniciales
A = [-2 1; 998 -999];
B = @(t) [2*sin(t); 999*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
nmax = 10;
TOL = .001;
N0 = 100;
h0 = 0.1;
jfunc = @(t,y) A;

%% Diagrama
% vector N, h
nVec = [];
hVec = [];
for j=1:8
    nVec(j)= N0*2^(j);
    hVec(j)=10/nVec(j);
end 

% vector error
eVec=[];
for k=nVec
    eVec = [eVec, mierrorglobal3(@mibdf,f,f_exact,intv,y0,k,TOL,nmax,jfunc)];
end

% comprobación error
p2 = sprintf('%g ', eVec);
fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', 'mibdf', p2)

%plot
figure1 = figure;
lines = ["*-", "*-", ">-", "^-"];

figure(figure1);
grid on;
loglog(hVec,eVec,lines{1});
hold on;
%% mibdf vs mitrapnwt

methods = {@mibdf, @mitrapnwt};
names = {'mibdf', 'mitrapnwt'};
k = 'Sistema Rígido';

miplot3(methods,names,k,f,f_exact,intv,y0,N0,h0,TOL,nmax,jfunc)


%% Apéndice: Las funciones Leap_frog y BDF2

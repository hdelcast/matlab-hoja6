function miplot(methods,names,k,f,f_exact,intv,y0,N0,h0,TOL,nmax,jfunc)

% Funciones de los métodos y sus nombres
mFuncs = methods;
mNames = names;

% inicialización vectores
nVec = N0;
hVec = h0;
errorVec = zeros(1,8);

for j=1:8
    nVec(j)=N0*2^(j);
    hVec(j)=10/nVec(j);
end

% comprobación vectores solución
fprintf('\n #VECTORES \n \n')
p11 = sprintf('%g ', nVec);
fprintf('El vector nVec tiene valor: [%s]\n', p11)
p12 = sprintf('%g ', hVec);
fprintf('El vector hVec tiene valor: [%s]\n ', p12)
fprintf('\n #ERRORES \n \n')

% figuras
figure1 = figure;
lines = ["*-", "*-", ">-", "^-"];

% bucle figura
for j=1:length(mFuncs)
    for i=1:length(nVec)

        % cálculo error global junto con número de evaluaciones
        [error, eval] = mierrorglobal(mFuncs{j},f,f_exact,intv,y0,nVec(i),TOL,nmax,jfunc);
        errorVec(i) = error;

    end
    %comprobación error
    p2 = sprintf('%g ', errorVec);
    fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', mNames{j}, p2)

    %plot
    figure(figure1);
    grid on;
    loglog(hVec,errorVec,lines{j});
    hold on;

    % pendiente
    pendiente = (log(errorVec(end))-log(errorVec(1)))/(log(hVec(end))-log(hVec(1)));
    s = sprintf('La pendiente para el método "%s" es: %g',mNames{j}, pendiente);
    fprintf(s);

    fprintf('\n \n')
    
end

% títulos, legendas y labels
xx = 'Error max';
yy = 'h';

figure(figure1);

if length(mNames) > 1
    legend(mNames{1},mNames{2},'Location','northeast');
    ss = sprintf('%s \n %s vs %s \n intv=[%g %g] y0=[%g %g] \n N_{int}=%g, TOL=%g, nmax=%g',k,xx,yy,intv,y0,nVec(1),TOL,nmax);
else
    legend(mNames{1},'Location','northeast');
    ss = sprintf('%s \n %s vs %s \n intv=[%g %g] y0=[%g %g] \n N_{int}=%g, TOL=%g, nmax=%g',k,xx,yy,intv,y0,nVec(1),TOL,nmax);
end

title(ss);
ylabel(xx);
xlabel(yy);

end
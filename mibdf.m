function [t,Y,ev,loopcount]=mibdf(f,intv,y0,N,TOL,nmax,jfunc)

ev = 0;
loopcount = 0;

a = intv(1);
b = intv(end);
h = (b - a)/N;
t = a:h:b;

m = size(y0, 1);
Y = zeros(m, length(t));

ev = 0;
loopcount = 0;

Y(:,1) = y0;

% init mieulerimpnwt
[~,Y] = mieulerimpnwt(f,intv,y0,N,TOL,nmax,jfunc);

for i=1:N-1
    y = Y(:,i);
    loop = 0;
    dif = 1;

    while (dif >= h^3) && (loop < nmax)

        % función auxiliar
        F = y - (4/3) * Y(:,i+1) + (1/3) * Y(:,i) - (2/3) * h * f(t(i+2),y);

        % jacobiano
        Jf = eye(length(y0)) - (2/3) * h * feval(jfunc, t(i+2), y);
        
        % Newton
        w = Jf\F;
        y = y - w;
        dif = norm(w);

        % evaluaciones y bucles
        loop = loop + 1;
        ev = ev + 1;

    end 
    Y(:, i+2) = y;
    loopcount = loopcount + loop;
end
end
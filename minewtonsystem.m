function [y]=minewtonsystem(f,Jf,y0,TOL,nmax)

y = y0;
loop = 0;
ev = 0;
dif = 1;
dif2 = 1;

while (dif > TOL) && loop < nmax
    k = y' - f(y)'/Jf(y);
    %k = y - f(y)/Jf;
    dif = max(max(abs(y-k)));
    %dif2 = max(max(k));
    y = k';

end
end
function [t,Y] = mileapfrog(f,intv,y0,N)

% datos
a = intv(1);
b = intv(2);
h = (b - a)/N;

% vect ini
t = a:h:b;
l = length(t);
m = size(y0,1);
Y = zeros(m,l);
Y(:,1) = y0;

% eul mod
f1 = feval(f,t(1),Y(:,1));
y1 = Y(:,1) + (h/2) * (f1 + feval(f, t(2),Y(:,1) + h * f1));

Y(:,2) = y1; 

% leap frog
for i = 3:N
    Y(:,i) =  Y(:,i-2) + 2 * h * feval(f,t(i-1),Y(:,i-1)); 

end
end
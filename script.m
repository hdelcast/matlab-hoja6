A = [-2 1; 998 -999];
B = @(t) [2*sin(t); 999*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
nmax = 100;
TOL = .01;
N0 = 100;
h0 = 0.1;
jfunc = @(t) A;

a = intv(1);
b = intv(end);
h = (b - a)/N0;

t = a;
y = y0;

T = t;
Y = y;

% initialization using backward Euler method
t = t+h;
y1 = fsolve(@(y) y - y0 - h*f(t,y), y0);

Y = [ Y, y1 ];
T = [ T, t ];

% bdf
for i=2:N0

    % t
    t = t + h;
    
    % función auxiliar
    F = @(z) z - (4/3)*Y(:,i) + (1/3)*Y(:,i-1) - (2/3)*h*f(t,z);

    % jacobiano
    Jf = @(z) (2/3)*h*eye(length(z)) - (4/3)*h*jfunc(z);

    % iteración newton
    y=minewtonsystem(F,Jf,y,TOL,nmax);
    y
    % vectores
    T = [T, t];
    Y = [Y, y];
    break

end

%%

% vector N, h
nVec = [];
hVec = [];
for j=1:8
    nVec(j)= N0*2^(j);
    hVec(j)=10/nVec(j);
end 

% vector error
eVec=[];
for k=nVec
    eVec = [eVec, mierrorglobal3(@mibdf,f,f_exact,intv,y0,k,TOL,nmax,jfunc)];
end

% comprobación error
p2 = sprintf('%g ', eVec);
fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', 'mileapfrog', p2)

%%

%plot
figure1 = figure;
lines = ["*-", "*-", ">-", "^-"];

figure(figure1);
grid on;
loglog(hVec,eVec,lines{1});
hold on;
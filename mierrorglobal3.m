function [error] = mierrorglobal3(met,f,f_exact,intv,y0,N,TOL,nmax,jfunc)

% cálculo y con método
[T,Y] = met(f,intv,y0,N,TOL,nmax,jfunc);

y_exact = f_exact(T);

error = max(max(abs(y_exact - Y)));

end